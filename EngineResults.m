//
//  EngineResults.m
//  GProofread
//
//  Created by Johnny on 21/03/2014.
//  Copyright (c) 2014 Johnny. All rights reserved.
//

#import "EngineResults.h"
#import "TFHpple.h"
#import "Track.h"
#import "Paper.h"

@implementation EngineResults

- (id)initWithTrack:(Track*)trackFromDelegate
{
    self = [super init];
    if (self)
    {
        self.keyWord = [[NSString alloc] initWithString:trackFromDelegate.wKey];
        self.year = [[NSString alloc] initWithString:trackFromDelegate.year];
    }
    return self;
}

- (NSInteger)pageNo:(TFHpple*)parse {
    
    NSString *xpathQueryPageNo = @"//div[@id='gs_ab_md']";
    NSArray *pageNo = [parse searchWithXPathQuery:xpathQueryPageNo];
    NSMutableString *totalResults;
    NSInteger number = 0;

    //calculate page no
    for (TFHppleElement *element in pageNo) {
        totalResults = [[NSMutableString alloc] initWithString:[[element firstChild] content]];
        
    }
    NSUInteger cLength = [totalResults length];
    NSRange cRange = NSMakeRange(0, cLength);
    NSInteger tmpLocBeg, tmpLocEnd;
    
    //cRange = [totalResults rangeOfString: @"About" options:0 range:cRange];
    if ([totalResults rangeOfString: @"About" options:0 range:cRange].location == NSNotFound) {
        cRange = [totalResults rangeOfString: @" " options:0 range:cRange];
        if(cRange.location != NSNotFound)
            cRange = NSMakeRange(cRange.location + cRange.length,
                                 cLength - (cRange.location + cRange.length));
        tmpLocBeg = 0;
        tmpLocEnd = cRange.location - 2;
        NSRange noRange = NSMakeRange(tmpLocBeg, tmpLocEnd-tmpLocBeg+1);
        NSString *pageNumber = [totalResults substringWithRange:noRange];
        number =[pageNumber integerValue];
        return (number/10)+1;
    }
    else{
        for (int i=0; i<2; i++) {
            cRange = [totalResults rangeOfString: @" " options:0 range:cRange];
            if(cRange.location != NSNotFound)
                cRange = NSMakeRange(cRange.location + cRange.length,
                                     cLength - (cRange.location + cRange.length));
            if (i==0) tmpLocBeg = cRange.location;
            else tmpLocEnd = cRange.location-2;
        }
        
        NSRange noRange = NSMakeRange(tmpLocBeg, tmpLocEnd-tmpLocBeg+1);
        NSString *pageNumber = [totalResults substringWithRange:noRange];
        number =[pageNumber  integerValue] ;
        return (number/10)+1;
    }
    return number;
}

- (NSMutableArray*)analyseURL {
    //scholar.google.co.uk/scholar?as_vis=1&q=%22computational+steering%22&hl=en&as_sdt=1,5&as_ylo=2001&as_yhi=2001
    //scholar.google.co.uk/scholar?         q=%22computational+steering%22&hl=en&as_sdt=1%2C5&as_vis=1&as_ylo=1993&as_yhi=1993
    //http:// scholar.google.co.uk/scholar?start=10&q=%22computational+steering%22&hl=en&as_sdt=1,5&as_ylo=2000&as_yhi=2000&as_vis=1
    
    NSUInteger length = [self.keyWord length];
    int spaceCount = 0;
    NSRange range = NSMakeRange(0, length);
    
    NSMutableString *url_prefix = [[NSMutableString alloc] initWithString:@"http://scholar.google.co.uk/scholar?start=0&q=%22"];
    NSMutableString *paperName = [[NSMutableString alloc] initWithString:self.keyWord];//paperName from user, paper.tile from GS
    
    //extract the name and replace " " with "+"
    while(range.location != NSNotFound)
    {
        range = [paperName rangeOfString: @" " options:0 range:range];
        if(range.location != NSNotFound)
        {
            spaceCount++;
            [paperName replaceCharactersInRange:range withString:@"+"];
            range = NSMakeRange(range.location + range.length, length - (range.location + range.length));
        }
    }
    [url_prefix appendString:paperName];
    [url_prefix appendString:@"%22&hl=en&as_sdt=1,5"];
    
    //restrain the year section
    [url_prefix appendString:@"&as_ylo="];
    [url_prefix appendString:self.year];
    [url_prefix appendString:@"&as_yhi="];
    [url_prefix appendString:self.year];
    [url_prefix appendString:@"&as_vis=1"];
    
    
    NSMutableArray *paperArray = [[NSMutableArray alloc] initWithCapacity:0];
    NSInteger currentPage = -1;
    while (true) {
        NSUInteger uLength = [url_prefix length];
        NSRange uRange = NSMakeRange(0, uLength);
        NSRange uRange_1 = [url_prefix rangeOfString: @"=" options:0 range:uRange];
        NSRange uRange_2 = [url_prefix rangeOfString: @"&" options:0 range:uRange];
        NSRange startPos = NSMakeRange(uRange_1.location+1, (uRange_2.location - uRange_1.location - 1));
        currentPage++;
        
        //[url_prefix replaceCharactersInRange:startPos withString:[NSString stringWithFormat: @"%d", (int)(currentPage*10)]];
        [url_prefix deleteCharactersInRange:startPos];
        [url_prefix insertString:[NSString stringWithFormat: @"%d", (int)(currentPage*10)] atIndex:(startPos.location)];
        //send the request
        NSURL *url = [NSURL URLWithString:url_prefix];
        NSURLRequest *request = [NSURLRequest requestWithURL: url];
        
        //Async
        //NSURLConnection *theConnection=[[NSURLConnection alloc] initWithRequest:request delegate:self];
        
        //Sync
        NSURLResponse * response = nil;
        NSError * error = nil;
        sleep(1);
        NSData * responseData = [NSURLConnection sendSynchronousRequest:request
                                                      returningResponse:&response
                                                                  error:&error];
        
        TFHpple *htmlParser = [TFHpple hppleWithHTMLData:responseData];
        
        NSString *xpathQueryTitle = @"//h3[@class='gs_rt']/a";
        NSString *xpathQueryCited = @"//div[@class='gs_fl']/a[1]";
        //NSString *xpathQuery
        
        NSArray *titleNodes = [htmlParser searchWithXPathQuery:xpathQueryTitle];
        NSArray *citedNodes = [htmlParser searchWithXPathQuery:xpathQueryCited];
        
        if ([titleNodes count] == 0) {
            return paperArray;
        }
        
        int i = 0;
        for (TFHppleElement *element in titleNodes) {
            // 5
            Paper *returnedPaper = [[Paper alloc] init];
            [paperArray addObject:returnedPaper];
            
            // 6
            NSArray *children = [element children];
            //NSLog(@"NO of children is %lu", (unsigned long)[children count]);
            //NSLog(@"1: %@", [[children objectAtIndex:0] content]);
            //NSLog(@"2: %@", [[[children objectAtIndex:1] firstChild] content]);
            
            //NSMutableString *nimaya = [[NSMutableString alloc] initWithString:@""];
            
            for (int j=0; j<(int)[children count]; j++) {
                if ([[children objectAtIndex:j] content]!=nil) {
                    NSString *tmpTitle = [[NSString alloc] initWithString:[[children objectAtIndex:j] content]];
                    if (j==0) returnedPaper.title = [NSMutableString stringWithString:tmpTitle];
                    else [returnedPaper.title appendString:tmpTitle];
                }
                else {
                    NSString *tmpTitle = [[NSString alloc] initWithString:[[[children objectAtIndex:j] firstChild] content]];
                    if (j==0) returnedPaper.title = [NSMutableString stringWithString:tmpTitle];
                    else [returnedPaper.title appendString:tmpTitle];
                }
            }
        }
        
        for (TFHppleElement *element in citedNodes) {
            NSMutableString *tmpContent = [[NSMutableString alloc] initWithString:[[element firstChild] content]];
            
            NSRange testRange = NSMakeRange(0, [tmpContent length]);
            testRange = [tmpContent rangeOfString:@"Cited" options:0 range:testRange];
            if (testRange.location == NSNotFound) {
                continue;
            }
            [[paperArray objectAtIndex:i] assignCitedNumber:tmpContent];
            i++;
        }

    }
    
    
    //NSLog(@"papers per page is:%ld", (long)pageCount);
    return paperArray;
}
/*
- (void)connection:(NSURLConnection *)connection didReceiveResponse:(NSURLResponse *)response
{
    self.responseData = [[NSMutableData alloc] init];
    NSLog(@"Got Response!");
}

- (void)connection:(NSURLConnection *)connection didReceiveData:(NSData *)data {
    // Append the new data to the instance variable you declared
    [self.responseData appendData:data];
    NSLog(@"Getting Data...");
}

- (NSCachedURLResponse *)connection:(NSURLConnection *)connection
                  willCacheResponse:(NSCachedURLResponse*)cachedResponse {
    // Return nil to indicate not necessary to store a cached response for this connection
    return nil;
}

- (void)connectionDidFinishLoading:(NSURLConnection *)connection {
    // The request is complete and data has been received
    // You can parse the stuff in your instance variable now
    NSLog(@"Finished Loading!");
    NSString *response = [[NSString alloc]initWithData:(self.responseData) encoding:NSUTF8StringEncoding];
    NSLog(@"the content is: %@", response);
}

- (void)connection:(NSURLConnection *)connection didFailWithError:(NSError *)error {
    // The request has failed for some reason!
    // Check the error var
}*/
//////////////////////////////////////////////////////////////////////////////////////////////////

@end
