//
//  HtmlToXml.h
//  GProofread
//
//  Created by Johnny on 26/03/2014.
//  Copyright (c) 2014 Johnny. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface HtmlToXml : NSObject

-(NSData*)convert:(NSString*)rawHtml;

@end
