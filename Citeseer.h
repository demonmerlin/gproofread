//
//  Citeseer.h
//  GProofread
//
//  Created by Johnny on 26/03/2014.
//  Copyright (c) 2014 Johnny. All rights reserved.
//

#import <Foundation/Foundation.h>

@class Track;
@class TFHpple;

@interface Citeseer : NSObject

@property NSString *gsWord;
@property NSString *gsYear;
@property NSMutableString *paperTitle;

- (id)initWithTrack:(Track*)trackFromDelegate;
- (void)verifiedByCiteseer:(NSMutableArray*)results;
- (void)analyseURL_CiteseerAt:(NSInteger)findNo andSetResults:(NSMutableArray*)results;
@end
