//
//  comparator.m
//  GProofread
//
//  Created by Johnny on 24/03/2014.
//  Copyright (c) 2014 Johnny. All rights reserved.
//

#import "comparator.h"

@implementation comparator

- (BOOL) compareTitleFromGS:(NSMutableString*)GStitle with:(NSString*)vTitle{
    NSMutableString *GSPaperTitle = [[NSMutableString alloc] initWithString:[GStitle lowercaseString]];
    NSMutableString *othersTitle = [[NSMutableString alloc] initWithString:[vTitle lowercaseString]];
    
    if([GSPaperTitle isEqualToString:othersTitle])return YES;
    else return NO;
}
- (BOOL) comparePYFromGS:(NSString*)GSPY with:(NSMutableString*)vPY{
    NSInteger gsPY = [GSPY integerValue];
    NSInteger otherPY = [vPY integerValue];
    if (gsPY == otherPY) return YES;
    else return NO;
}

@end
