//
//  Paper.m
//  GProofread
//
//  Created by Johnny on 21/03/2014.
//  Copyright (c) 2014 Johnny. All rights reserved.
//

#import "Paper.h"

@implementation Paper

- (void) assignCitedNumber:(NSMutableString*)citedNumber{
    NSUInteger cLength = [citedNumber length];
    NSRange cRange = NSMakeRange(0, cLength);
    NSRange tmpRange;
    
    
    while(cRange.location != NSNotFound)
    {
        cRange = [citedNumber rangeOfString: @" " options:0 range:cRange];
        if(cRange.location != NSNotFound)
        {
            cRange = NSMakeRange(cRange.location + cRange.length,
                                 cLength - (cRange.location + cRange.length));
        }
        else break;
        tmpRange = cRange;
    }
    
    NSString *cited = [citedNumber substringWithRange:tmpRange];
    
    self.cited  = [cited integerValue];
}

- (void) printPaper{
    NSLog(@"Title: %@", self.title);
    NSLog(@"Cited by %ld", (long)self.cited);
}

@end
