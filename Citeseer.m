//
//  Citeseer.m
//  GProofread
//
//  Created by Johnny on 26/03/2014.
//  Copyright (c) 2014 Johnny. All rights reserved.
//

#import "Citeseer.h"
#import "Track.h"
#import "TFHpple.h"
#import "comparator.h"
#import "HtmlToXml.h"

@implementation Citeseer

- (id)initWithTrack:(Track*)trackFromDelegate
{
    self = [super init];
    if (self)
    {
        self.gsWord = [[NSString alloc] initWithString:trackFromDelegate.wKey];
        self.gsYear = [[NSString alloc] initWithString:trackFromDelegate.year];
    }
    return self;
}

- (void)analyseURL_CiteseerAt:(NSInteger)findNo andSetResults:(NSMutableArray*)results{
    //citeseerx.ist.psu.edu/search?q=%22computational+steering%22&submit=Search&sort=rlv&t=doc
    NSMutableString *suspectTitle = [[NSMutableString alloc] initWithString:self.paperTitle];
    NSInteger loopNO = findNo;
    comparator *spgComp = [comparator new];
    NSString *url_prefix = @"http://citeseerx.ist.psu.edu/search?q=\"";
    NSMutableString *url_address = [[NSMutableString alloc] initWithString:[url_prefix stringByAppendingString:suspectTitle]];
    [url_address appendString:@"\"&submit=Search&sort=rlv&t=doc"];
    NSString *properlyEscapedURL = [url_address stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
    
    NSURL *url = [NSURL URLWithString:properlyEscapedURL];
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL: url];
    
    //Sync
    NSURLResponse * response = nil;
    NSError * error = nil;
    sleep(1);
    NSData * responseData = [NSURLConnection sendSynchronousRequest:request
                                                  returningResponse:&response
                                                              error:&error];
    
    if (error != nil) {
        NSLog(@"error %@",error);
    }
    
    //NSString *searchResult2 = [[NSString alloc]initWithData:(responseData) encoding:NSUTF8StringEncoding];
    
    //HtmlToXml *convertor = [HtmlToXml new];
    //TFHpple *resultParser = [TFHpple hppleWithXMLData:[convertor convert:searchResult2]];
    TFHpple *resultParser = [TFHpple hppleWithHTMLData:responseData];
    NSString *xpathQueryFound = @"//div[@id='result_info']/strong";
    NSString *xpathQueryTitle = @"//div[@id='result_list']/div[@class='result']/h3/a";
    NSString *xpathQueryYear = @"//div[@id='result_list']/div[@class='result']/div[@class='pubinfo']/span[@class='pubyear']";
    
    NSArray *foundNodes = nil;
    foundNodes = [resultParser searchWithXPathQuery:xpathQueryFound];
    NSArray *returnNodes = [resultParser searchWithXPathQuery:xpathQueryTitle];
    NSArray *yearNodes = [resultParser searchWithXPathQuery:xpathQueryYear];
    
    NSMutableString *foundReuslts = [NSMutableString new];
    if ([foundNodes count] == 0){
        NSLog(@"Request Error in Springer line 73");
        return;
    }else{
        for (TFHppleElement *element in foundNodes) {
            [foundReuslts setString:[[element firstChild] content]];
            if ([foundReuslts isEqualToString:@"No results found"]) {
                NSLog(@"Didn't find!");
                return;
            }
        }
    }
    NSMutableString *vTitle = [[NSMutableString alloc] initWithString:@""];
    int yearCount = 0;
    for (TFHppleElement *element in returnNodes) {
    //TFHppleElement *element = [returnNodes firstObject];
        NSArray *children = [element children];
        for (int j=0; j<(int)[children count]; j++) {
            if ([[children objectAtIndex:j] content]!=nil) {
                NSString *tmpTitle = [[NSString alloc] initWithString:[[children objectAtIndex:j] content]];
                if (j==0) {
                    vTitle = [NSMutableString stringWithString:tmpTitle];
                }
                else {
                    [vTitle appendString:tmpTitle];
                }
            }
            else {
                NSString *tmpTitle = [[NSString alloc] initWithString:[[[children objectAtIndex:j] firstChild] content]];
                if (j==0) {
                    vTitle = [NSMutableString stringWithString:tmpTitle];
                }
                else {
                    [vTitle appendString:tmpTitle];
                }
            }
        }

        NSUInteger length = [vTitle length];
        NSRange range = NSMakeRange(0, length);
        while(range.location != NSNotFound)
        {
            range = [vTitle rangeOfString: @"\n" options:0 range:range];
            if(range.location != NSNotFound)
            {
                range = NSMakeRange(range.location, range.length+17);
                [vTitle deleteCharactersInRange:range];
                length = [vTitle length];
                range = NSMakeRange(0, length);
            }
        }
        [vTitle deleteCharactersInRange:NSMakeRange(([vTitle length]-1), 1)];
        [vTitle deleteCharactersInRange:NSMakeRange(0, 1)];
        //NSLog(@"title is:%@",vTitle);
        
        if([spgComp compareTitleFromGS:suspectTitle with:vTitle]){
            NSLog(@"Found %@ in CiteseerX",vTitle);
            [[results objectAtIndex:loopNO] replaceObjectAtIndex:1 withObject:@"YES"];
            int forCount = 0;
            for (TFHppleElement *yearElement in yearNodes) {
                if (forCount==yearCount) {
                    NSMutableString *unkownValue = [[NSMutableString alloc] initWithString:[[yearElement firstChild] content]];
                    [unkownValue deleteCharactersInRange:NSMakeRange(0, 2)];
                    if ([spgComp comparePYFromGS:self.gsYear with:unkownValue]) {
                        [[results objectAtIndex:loopNO] replaceObjectAtIndex:2 withObject:@"YES"];
                        NSLog(@"The Published Year is Matched!!");
                    }
                }
                forCount++;
            }
        }
        //NSLog(@"The search result is: %@", wosResults);
        yearCount++;
    }
    
}

- (void)verifiedByCiteseer:(NSMutableArray*)results{
    int findNo = 0;
    for (NSMutableArray *paperArray in results) {
        if ([[paperArray objectAtIndex:1] isEqualToString:@"YES"]) {
            findNo++;
            continue;
        }
        self.paperTitle = [NSMutableString stringWithString:[paperArray objectAtIndex:0]];
        NSLog(@"Begin to find %d:%@",findNo+1,self.paperTitle);
        [self analyseURL_CiteseerAt:findNo andSetResults:results];
        findNo++;
    }
    
    NSLog(@"Finishing Comparing");
}

@end
