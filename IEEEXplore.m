//
//  IEEEXplore.m
//  GProofread
//
//  Created by Johnny on 23/03/2014.
//  Copyright (c) 2014 Johnny. All rights reserved.
//

#import "IEEEXplore.h"
#import "TFHpple.h"
#import "Track.h"
#import "Paper.h"
#import "comparator.h"

@implementation IEEEXplore

- (id)initWithTrack:(Track*)trackFromDelegate
{
    self = [super init];
    if (self)
    {
        self.gsWord = [[NSString alloc] initWithString:trackFromDelegate.wKey];
        self.gsYear = [[NSString alloc] initWithString:trackFromDelegate.year];
    }
    return self;
}

- (void)analyseURL_IEEEAt:(NSInteger)findNo andSetResults:(NSMutableArray*)results{
    //ieeexplore.ieee.org/search/searchresult.jsp?newsearch=true&queryText=Cavestudy%3A+an+infrastructure+for+computational+steering+in+virtual+reality+environments
    //ieeexplore.ieee.org/gateway/ipsSearch.jsp?querytext=java&au=Wang&hc=10&rs=11&sortfield=ti&sortorder=asc
    
    NSMutableString *suspectTitle = [[NSMutableString alloc] initWithString:self.paperTitle];
    
    NSUInteger length = [suspectTitle length];
    NSInteger loopNO = findNo;
    int spaceCount = 0;
    NSRange range = NSMakeRange(0, length);
    
    NSMutableString *url_prefix = [[NSMutableString alloc] initWithString:@"http://ieeexplore.ieee.org/gateway/ipsSearch.jsp?querytext="];

    while(range.location != NSNotFound)
    {
        if (suspectTitle == nil) {
            [NSException raise:@"Paper title is none" format:@"None result found in GS!"];
        }
        range = [suspectTitle rangeOfString: @" " options:0 range:range];
        if(range.location != NSNotFound)
        {
            spaceCount++;
            [suspectTitle replaceCharactersInRange:range withString:@"+"];
            range = NSMakeRange(range.location + range.length, length - (range.location + range.length));
        }
    }
    [url_prefix appendString:suspectTitle];
    
    //send the request
    NSURL *url = [NSURL URLWithString:url_prefix];
    //NSData *data = [NSData dataWithContentsOfURL:url];
    
    NSURLRequest *request = [NSURLRequest requestWithURL: url];
    
    //Async
    //NSURLConnection *theConnection=[[NSURLConnection alloc] initWithRequest:request delegate:self];
    
    //Sync
    NSURLResponse * response = nil;
    NSError * error = nil;
    NSData * responseData = [NSURLConnection sendSynchronousRequest:request
                                                  returningResponse:&response
                                                              error:&error];
    
    NSXMLParser *xmlParser = [[NSXMLParser alloc]initWithData:responseData];
    
    [xmlParser setDelegate:self];
    self.titleFlag = NO;
    self.yearFlag = NO;
    self.ieeePaperArray = [[NSMutableArray alloc] initWithCapacity:0];
    //self.ieeeYearArray = [[NSMutableArray alloc] initWithCapacity:0];
    BOOL result = [xmlParser parse];
    if (result == NO) {
        NSLog(@"💙 There is an error when parsing this paper");
    }
    
    comparator *comp = [comparator new];
    /*if ([self.ieeeTitleArray count] != [self.ieeeYearArray count]) {
        NSLog(@"💙Title array doesn't match Year Array! Title array %lu year array %lu", [self.ieeeTitleArray count], [self.ieeeYearArray count]);
    }*/
    for (int arrayLength=0; arrayLength<[self.ieeePaperArray count]; arrayLength++) {
        if([comp compareTitleFromGS:self.paperTitle with:[[self.ieeePaperArray objectAtIndex:arrayLength] objectAtIndex:0]])
        {
            [[results objectAtIndex:(loopNO-1)] replaceObjectAtIndex:1 withObject:@"YES"];
            NSLog(@"Found %@ in IEEEXplore",[self.ieeePaperArray objectAtIndex:arrayLength]);
            if ([comp comparePYFromGS:self.gsYear with:[[self.ieeePaperArray objectAtIndex:arrayLength] objectAtIndex:1]]) {
                NSLog(@"The Published Year is matched");
                [[results objectAtIndex:(loopNO-1)] replaceObjectAtIndex:2 withObject:@"YES"];
            }
            else NSLog(@"The Published Year isn't mached!");
        }
    }
    return;
}

- (void) verifiedByIEEE:(NSMutableArray*)gsResults andSetResults:(NSMutableArray*)results {
    int findNo = 0;
    for (Paper *gsPaper in gsResults) {
        findNo++;
        self.paperTitle = [NSMutableString stringWithString:gsPaper.title];
        NSLog(@"Begin to find %d:%@",findNo,self.paperTitle);
        [self analyseURL_IEEEAt:findNo andSetResults:results];
    }
     NSLog(@"Finishing Comparing");
}

- (void)parser:(NSXMLParser *)parser didStartElement:(NSString *)elementName namespaceURI:(NSString *)namespaceURI qualifiedName:(NSString *)qualifiedName attributes:(NSDictionary *)attributeDict
{
    //NSLog(@"Did start element\n");
    if ([elementName isEqualToString:@"title"])
    {
        self.titleFlag = YES;
        return;
    }
    else if([elementName isEqualToString:@"py"])
    {
        self.yearFlag = YES;
    }
}
    
- (void)parser:(NSXMLParser *)parser didEndElement:(NSString *)elementName namespaceURI:(NSString *)namespaceURI qualifiedName:(NSString *)qName
{}

- (void)parser:(NSXMLParser *)parser foundCharacters:(NSString *)string
{
    //NSLog(@"GS  :%@", self.paperTitle);
    //NSLog(@"IEEE:%@", string);
    if (self.titleFlag == YES) {
        //[self.ieeeTitleArray addObject:string];
        self.ieeeTitle = @"";
        self.ieeeTitle = string;
        self.titleFlag = NO;
    }
    if (self.yearFlag == YES) {
        //[self.ieeeYearArray addObject:string];
        self.ieeeYear = @"";
        self.ieeeYear = string;
        NSArray *atom = [[NSArray alloc] initWithObjects:self.ieeeTitle,self.ieeeYear,nil];
        [self.ieeePaperArray addObject:atom];
        self.yearFlag = NO;
    }
    /*
    comparator *comp = [comparator new];
    if([comp compareTitleFromGS:self.paperTitle with:string])
    {
        NSLog(@"Found %@ in IEEEXplore",string);
    }*/
}

@end
