//
//  AppDelegate.m
//  GProofread
//
//  Created by Johnny on 28/02/2014.
//  Copyright (c) 2014 Johnny. All rights reserved.
//

#import "AppDelegate.h"
#import "Track.h"
#import "TFHpple.h"
#import "EngineResults.h"
#import "IEEEXplore.h"
#import "WebOfScience.h"
#import "Springer.h"
#import "Paper.h"
#import "Citeseer.h"
#import "Scopus.h"

@implementation AppDelegate

- (void)applicationDidFinishLaunching:(NSNotification *)aNotification
{
    // Insert code here to initialize your application
    Track *aTrack = [[Track alloc] init];
    NSMutableArray * years = [[NSMutableArray alloc] init];
    for (int i=0; i<24; i++) {
        [years addObject:[NSString stringWithFormat:@"%d",1990+i]];
    }
    
    [self setTrack:aTrack];
    [self.popUpBotton removeAllItems];
    [self.popUpBotton addItemsWithTitles:years];
}

- (IBAction)getTextField:(id)sender {
    NSString *keyword = [self.textField stringValue];
    [self.track setWKey:keyword];
}

- (IBAction)getPopUp:(id)sender {
    //int index = [sender indexOfObject:sender];
    
}

- (IBAction)searchNcompare:(id)sender {
    NSString *keyword = [self.textField stringValue];
    [self.track setWKey:keyword];
    
    NSString *year = [self.popUpBotton titleOfSelectedItem];
    [self.track setYear:year];
    
    EngineResults *er = [[EngineResults alloc] initWithTrack:self.track];
    NSMutableArray *response = [er analyseURL];
    self.track.gsArray = response;
    self.track.results = [[NSMutableArray alloc] initWithCapacity:0];
    for (int resultLength=0; resultLength<[response count]; resultLength++) {
        NSMutableArray *result = [[NSMutableArray alloc] initWithObjects:[[response objectAtIndex:resultLength] title],@"NO",@"NO", nil];//name, if found, if matched
        [self.track.results addObject:result];
    }
    
    for (Paper *finalResult in response) {
        [finalResult printPaper];
    }
    
    NSLog(@"Total number is: %lu", (unsigned long)[response count]);
}

- (void)printResults{
    int countUnFound = 0;
    int countFound = 0;
    int countMisMatched = 0;
    
    for (NSMutableArray *singelResult in self.track.results) {
        NSLog(@"%@",[singelResult objectAtIndex:0]);
        NSLog(@"Found: %@ , Matched: %@",[singelResult objectAtIndex:1], [singelResult objectAtIndex:2]);
    }
    
    NSLog(@"💙 The unfound papers are:");
    for (NSMutableArray *unfoundResult in self.track.results) {
        if ([[unfoundResult objectAtIndex:1] isEqualToString:@"NO"]) {
            NSLog(@"%@",[unfoundResult objectAtIndex:0]);
            countUnFound++;
        }else if([[unfoundResult objectAtIndex:1] isEqualToString:@"YES"]){
            countFound++;
        }
    }
    
    NSLog(@"💙 The mismatched papers are:");
    for (NSMutableArray *mismatchedResult in self.track.results) {
        if ([[mismatchedResult objectAtIndex:1] isEqualToString:@"YES"] &&[[mismatchedResult objectAtIndex:2] isEqualToString:@"NO"]){
            NSLog(@"%@",[mismatchedResult objectAtIndex:0]);
            countMisMatched++;
        }
    }
    
    NSLog(@"💙 %d FOUND %d UNFOUND, %d MISMATHCED",countFound, countUnFound, countMisMatched);

}

- (IBAction)verify:(id)sender {
    /*
    //check in IEEEXplore first
    IEEEXplore *ieee = [[IEEEXplore alloc] initWithTrack:self.track];
    [ieee verifiedByIEEE:self.track.gsArray andSetResults:self.track.results];
    
    //check rest in Web of Science
    WebOfScience *wos = [[WebOfScience alloc] initWithTrack:self.track];
    [wos verifiedByWoS:self.track.results];
    
    //check rest in Springer
    Springer *spg = [[Springer alloc] initWithTrack:self.track];
    [spg verifiedBySpringer:self.track.results];
     
    //check rest in ScienceDirect/Scopus
    Scopus *scop = [[Scopus alloc] initWithTrack:self.track];
    [scop verifiedByScop:self.track.results];
    
    
    //check rest in Citeseer
    Citeseer *citer = [[Citeseer alloc] initWithTrack:self.track];
    [citer verifiedByCiteseer:self.track.results];
    
    //check rest in ACM DL
    */
    //print out verified results
    [self printResults];
    [self.found setValue:self.found forKey:@"haha"];
}

@end
