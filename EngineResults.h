//
//  EngineResults.h
//  GProofread
//
//  Created by Johnny on 21/03/2014.
//  Copyright (c) 2014 Johnny. All rights reserved.
//

#import <Foundation/Foundation.h>

@class Track;
@class TFHpple;

@interface EngineResults : NSObject

@property (nonatomic, copy) NSString *url;
@property NSString *keyWord;
@property NSString *year;

- (NSMutableArray*)analyseURL;
- (NSInteger)pageNo:(TFHpple*)parse;
- (id)initWithTrack:(Track*)trackFromDelegate;

@end
