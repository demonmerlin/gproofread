//
//  IEEEXplore.h
//  GProofread
//
//  Created by Johnny on 23/03/2014.
//  Copyright (c) 2014 Johnny. All rights reserved.
//

@class Track;
@class TFHpple;

#import <Foundation/Foundation.h>

@interface IEEEXplore : NSObject <NSXMLParserDelegate>

@property NSString *gsWord;
@property NSString *gsYear;
@property NSMutableString *paperTitle;
@property BOOL titleFlag;
@property BOOL yearFlag;
@property NSString *ieeeTitle;
@property NSString *ieeeYear;
@property NSMutableArray *ieeePaperArray;
@property NSMutableArray *ieeeYearArray;

- (void) verifiedByIEEE:(NSMutableArray*)gsResults andSetResults:(NSMutableArray*)results;
- (id)initWithTrack:(Track*)trackFromDelegate;
- (void)analyseURL_IEEEAt:(NSInteger)findNo andSetResults:(NSMutableArray*)results;
@end
