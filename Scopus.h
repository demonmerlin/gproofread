//
//  Scopus.h
//  GProofread
//
//  Created by Johnny on 27/03/2014.
//  Copyright (c) 2014 Johnny. All rights reserved.
//

#import <Foundation/Foundation.h>

@class Track;
@class TFHpple;

@interface Scopus : NSObject

@property NSString *gsWord;
@property NSString *gsYear;
@property NSMutableString *paperTitle;

- (id)initWithTrack:(Track*)trackFromDelegate;
- (void) verifiedByScop:(NSMutableArray*)results;
- (void)analyseURL_ScopAt:(NSInteger)findNo andSetResults:(NSMutableArray*)results;

@end
