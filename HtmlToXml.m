//
//  HtmlToXml.m
//  GProofread
//
//  Created by Johnny on 26/03/2014.
//  Copyright (c) 2014 Johnny. All rights reserved.
//

#import "HtmlToXml.h"

@implementation HtmlToXml

-(NSData*)convert:(NSString*)rawHtml{
    NSMutableString *repair = [[NSMutableString alloc] initWithString:rawHtml];
    //NSMutableString *repair = [[NSMutableString alloc] initWithString:@"womendoushihaohaizi&#034;woailaopozhouya&mdash;ngzidfhgdhtehrthe&#034;sfdsgdfbdfb"];
    NSUInteger length = [repair length];
    
    //These parameters need to be set to 0
    NSInteger lenOfStrangeChar = 0;
    NSInteger lenOfSubstitutor = 0;
    int count = 0;
    NSRange range = NSMakeRange(0, 0);
    /////////
    
    //!!!-----convert &mdash; to - -------!!!
    lenOfStrangeChar = [@"&mdash;" length];
    lenOfSubstitutor = [@"-" length];
    range = NSMakeRange(0, length);
    while(range.location != NSNotFound)
    {
        count ++;
        range = [repair rangeOfString: @"&mdash;" options:0 range:range];
        if(range.location != NSNotFound)
        {
            //spaceCount++;
            [repair replaceCharactersInRange:range withString:@"-"];
            if (lenOfStrangeChar>lenOfSubstitutor) {
                range = NSMakeRange(range.location + lenOfSubstitutor, length - range.location - lenOfSubstitutor -count*(lenOfStrangeChar-lenOfSubstitutor));
            }
            else range = NSMakeRange(range.location + lenOfSubstitutor, length - range.location - lenOfSubstitutor +count*(lenOfSubstitutor - lenOfStrangeChar));
            //NSLog(@"%@",repair);
        }
    }
    lenOfStrangeChar = 0;
    lenOfSubstitutor = 0;
    count = 0;
    range = NSMakeRange(0, 0);
    length = [repair length];
    
    //!!!------convert &#034; to " -------!!!
    lenOfStrangeChar = [@"&#034;" length];
    lenOfSubstitutor = [@"\"" length];
    range = NSMakeRange(0, length);
    while(range.location != NSNotFound)
    {
        count ++;
        range = [repair rangeOfString: @"&#034;" options:0 range:range];
        if(range.location != NSNotFound)
        {
            //spaceCount++;
            [repair replaceCharactersInRange:range withString:@"\""];
            if (lenOfStrangeChar>lenOfSubstitutor) {
                range = NSMakeRange(range.location + lenOfSubstitutor, length - range.location - lenOfSubstitutor -count*(lenOfStrangeChar-lenOfSubstitutor));
            }
            else range = NSMakeRange(range.location + lenOfSubstitutor, length - range.location - lenOfSubstitutor +count*(lenOfSubstitutor - lenOfStrangeChar));
            //NSLog(@"%@",repair);
        }
    }
    lenOfStrangeChar = 0;
    lenOfSubstitutor = 0;
    count = 0;
    range = NSMakeRange(0, 0);
    length = [repair length];

    //!!!-----convert &amp; to & ------!!!!!
    lenOfStrangeChar = [@"&amp;" length];
    lenOfSubstitutor = [@"&" length];
    range = NSMakeRange(0, length);
    while(range.location != NSNotFound)
    {
        count ++;
        range = [repair rangeOfString: @"&amp;" options:0 range:range];
        if(range.location != NSNotFound)
        {
            //spaceCount++;
            [repair replaceCharactersInRange:range withString:@"&"];
            if (lenOfStrangeChar>lenOfSubstitutor) {
                range = NSMakeRange(range.location + lenOfSubstitutor, length - range.location - lenOfSubstitutor -count*(lenOfStrangeChar-lenOfSubstitutor));
            }
            else range = NSMakeRange(range.location + lenOfSubstitutor, length - range.location - lenOfSubstitutor +count*(lenOfSubstitutor - lenOfStrangeChar));
            //NSLog(@"%@",repair);
        }
    }
    lenOfStrangeChar = 0;
    lenOfSubstitutor = 0;
    count = 0;
    range = NSMakeRange(0, 0);
    length = [repair length];
    
    //!!!-----convert &copy; to © -----!!!!!
    lenOfStrangeChar = [@"&copy;" length];
    lenOfSubstitutor = [@"©" length];
    range = NSMakeRange(0, length);
    while(range.location != NSNotFound)
    {
        count ++;
        range = [repair rangeOfString: @"&copy;" options:0 range:range];
        if(range.location != NSNotFound)
        {
            //spaceCount++;
            [repair replaceCharactersInRange:range withString:@"©"];
            if (lenOfStrangeChar>lenOfSubstitutor) {
                range = NSMakeRange(range.location + lenOfSubstitutor, length - range.location - lenOfSubstitutor -count*(lenOfStrangeChar-lenOfSubstitutor));
            }
            else range = NSMakeRange(range.location + lenOfSubstitutor, length - range.location - lenOfSubstitutor +count*(lenOfSubstitutor - lenOfStrangeChar));
            //NSLog(@"%@",repair);
        }
    }
    
    
    //NSLog(@"The modified string is:\n"
    //      "%@",repair);
    NSData *modifiedXML = [repair dataUsingEncoding:NSUTF8StringEncoding];
    return modifiedXML;
}

@end
