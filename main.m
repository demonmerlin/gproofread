//
//  main.m
//  GProofread
//
//  Created by Johnny on 28/02/2014.
//  Copyright (c) 2014 Johnny. All rights reserved.
//

#import <Cocoa/Cocoa.h>

int main(int argc, const char * argv[])
{
    return NSApplicationMain(argc, argv);
}
