//
//  Springer.h
//  GProofread
//
//  Created by Johnny on 26/03/2014.
//  Copyright (c) 2014 Johnny. All rights reserved.
//

@class Track;
@class TFHpple;

#import <Foundation/Foundation.h>

@interface Springer : NSObject

@property NSString *gsWord;
@property NSString *gsYear;
@property NSMutableString *paperTitle;
@property BOOL titleFlag;
@property BOOL yearFlag;
@property NSString *springerTitle;
@property NSString *springerYear;
@property NSMutableArray *springerPaperArray;
@property NSMutableArray *springerYearArray;

- (id)initWithTrack:(Track*)trackFromDelegate;
- (void) verifiedBySpringer:(NSMutableArray*)results;
- (void)analyseURL_springerAt:(NSInteger)findNo andSetResults:(NSMutableArray*)results;

@end
