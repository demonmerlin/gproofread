//
//  Paper.h
//  GProofread
//
//  Created by Johnny on 21/03/2014.
//  Copyright (c) 2014 Johnny. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Paper : NSObject

@property NSMutableString* title;
@property NSInteger cited;

- (void) assignCitedNumber:(NSMutableString*)citedNumber;
- (void) printPaper;

@end
