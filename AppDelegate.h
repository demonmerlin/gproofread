//
//  AppDelegate.h
//  GProofread
//
//  Created by Johnny on 28/02/2014.
//  Copyright (c) 2014 Johnny. All rights reserved.
//

#import <Cocoa/Cocoa.h>

@class Track;

@interface AppDelegate : NSObject <NSApplicationDelegate>

//For Asynchronous 
@property NSMutableData *responseData;

@property (assign) IBOutlet NSWindow *window;
@property (weak) IBOutlet NSTextField *textField;
@property (strong) Track *track;
@property (weak) IBOutlet NSPopUpButton *popUpBotton;
@property (weak) IBOutlet NSBox *found;
@property (weak) IBOutlet NSBox *unfound;
@property (weak) IBOutlet NSBox *mismatched;

- (IBAction)getTextField:(id)sender;
- (IBAction)getPopUp:(id)sender;
- (IBAction)searchNcompare:(id)sender;
- (IBAction)verify:(id)sender;
- (void)printResults;

@end
