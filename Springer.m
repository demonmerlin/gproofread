//
//  Springer.m
//  GProofread
//
//  Created by Johnny on 26/03/2014.
//  Copyright (c) 2014 Johnny. All rights reserved.
//

#import "Springer.h"
#import "Track.h"
#import "TFHpple.h"
#import "comparator.h"

@implementation Springer

- (id)initWithTrack:(Track*)trackFromDelegate
{
    self = [super init];
    if (self)
    {
        self.gsWord = [[NSString alloc] initWithString:trackFromDelegate.wKey];
        self.gsYear = [[NSString alloc] initWithString:trackFromDelegate.year];
    }
    return self;
}

- (void)analyseURL_springerAt:(NSInteger)findNo andSetResults:(NSMutableArray*)results{
    //api.springer.com/metadata/pam?q=title:%22cse%22&api_key=7bc3fs7th8zky3k5yxmhnza3
    NSMutableString *suspectTitle = [[NSMutableString alloc] initWithString:self.paperTitle];
    NSString *api_key= @"\"&api_key=7bc3fs7th8zky3k5yxmhnza3";
    NSInteger loopNO = findNo;
    comparator *spgComp = [comparator new];
    NSString *url_prefix = @"http://api.springer.com/metadata/pam?q=title:\"";
    NSMutableString *url_address = [[NSMutableString alloc] initWithString:[url_prefix stringByAppendingString:suspectTitle]];
    [url_address appendString:api_key];
    NSString *properlyEscapedURL = [url_address stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
    
    NSURL *url = [NSURL URLWithString:properlyEscapedURL];
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL: url];
    
    //Sync
    NSURLResponse * response = nil;
    NSError * error = nil;
    sleep(1);
    NSData * responseData = [NSURLConnection sendSynchronousRequest:request
                                                  returningResponse:&response
                                                              error:&error];
    
    if (error != nil) {
        NSLog(@"error %@",error);
    }
    
    TFHpple *resultParser = [TFHpple hppleWithXMLData:responseData];
    NSString *xpathQueryFound = @"//total";
    NSString *xpathQueryTitle = @"//*[name()='dc:title']";
    NSString *xpathQueryYear = @"//*[name()='prism:publicationDate']";
    
    NSArray *foundNodes = nil;
    foundNodes = [resultParser searchWithXPathQuery:xpathQueryFound];
    NSArray *returnNodes = [resultParser searchWithXPathQuery:xpathQueryTitle];
    NSArray *yearNodes = [resultParser searchWithXPathQuery:xpathQueryYear];
    
    NSMutableString *foundReuslts = [NSMutableString new];
    for (TFHppleElement *element in foundNodes) {
        [foundReuslts setString:[[element firstChild] content]];
    }
    
    if ([foundReuslts isEqualToString:@"0"]) {
        NSLog(@"Didn't find!");
        return;
    }else if ([foundNodes count] == 0){
        NSLog(@"Request Error in Springer line 73");
        return;
    }
    int yearCount = 0;
    for (TFHppleElement *element in returnNodes) {
        NSString *vTitle = [[NSString alloc] initWithString:[[element firstChild] content]];
        if([spgComp compareTitleFromGS:suspectTitle with:vTitle]){
            NSLog(@"Found %@ in Springer",vTitle);
            [[results objectAtIndex:loopNO] replaceObjectAtIndex:1 withObject:@"YES"];
            int forCount = 0;
            for (TFHppleElement *element in yearNodes) {
                if (forCount==yearCount) {
                    NSMutableString *unkownValue = [[NSMutableString alloc] initWithString:[[element firstChild] content]];
                    if ([spgComp comparePYFromGS:self.gsYear with:unkownValue]) {
                        [[results objectAtIndex:loopNO] replaceObjectAtIndex:2 withObject:@"YES"];
                        NSLog(@"The Published Year is Matched!!");
                    }
                }
                //else NSLog(@"The Published Year isn't mached!");
                forCount++;
            }
        }
        //NSLog(@"The search result is: %@", wosResults);
        yearCount++;
    }
    
    //NSString *closeResult2 = [[NSString alloc]initWithData:(responseData) encoding:NSUTF8StringEncoding];
    //NSLog(@"The Springer result is: %@", closeResult2);
}

- (void) verifiedBySpringer:(NSMutableArray*)results{
    int findNo = 0;
    
    for (NSMutableArray *paperArray in results) {
        if ([[paperArray objectAtIndex:1] isEqualToString:@"YES"]) {
            findNo++;
            continue;
        }
        self.paperTitle = [NSMutableString stringWithString:[paperArray objectAtIndex:0]];
        NSLog(@"Begin to find %d:%@",findNo+1,self.paperTitle);
        [self analyseURL_springerAt:findNo andSetResults:results];
        findNo++;
    }
}

@end
