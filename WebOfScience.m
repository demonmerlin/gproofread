//
//  WebOfScience.m
//  GProofread
//
//  Created by Johnny on 25/03/2014.
//  Copyright (c) 2014 Johnny. All rights reserved.
//

#import "WebOfScience.h"
#import "Track.h"
#import "TFHpple.h"
#import "comparator.h"

@implementation WebOfScience

- (id)initWithTrack:(Track*)trackFromDelegate
{
    self = [super init];
    if (self)
    {
        self.gsWord = [[NSString alloc] initWithString:trackFromDelegate.wKey];
        self.gsYear = [[NSString alloc] initWithString:trackFromDelegate.year];
    }
    return self;
}

- (NSMutableString*)openSession{
    NSString *serviceEndPoint = @"http://search.webofknowledge.com/esti/wokmws/ws/WOKMWSAuthenticate/wdsl";
    NSString *actionAuthen = @"";
    NSString *soapMsg = [NSString stringWithFormat:@"<?xml version=\"1.0\" encoding=\"utf-8\"?>\n"
                         "<soapenv:Envelope xmlns:soapenv=\"http://schemas.xmlsoap.org/soap/envelope/\" xmlns:auth=\"http://auth.cxf.wokmws.thomsonreuters.com\">\n"
                         "<soapenv:Header/>\n"
                         "<soapenv:Body>\n"
                         "<auth:authenticate/>\n"
                         "</soapenv:Body>\n"
                         "</soapenv:Envelope>\n"];
    
    NSURL *soapUrl = [NSURL URLWithString: serviceEndPoint];
    NSMutableURLRequest *req = [NSMutableURLRequest requestWithURL:soapUrl];
    NSString *msgLength = [NSString stringWithFormat: @"%d", (int)[soapMsg length]];
    [req addValue: @"text/xml; charset=utf-8" forHTTPHeaderField: @"Content-Type"];
    [req addValue: actionAuthen forHTTPHeaderField: @"SOAPAction"];
    [req addValue: msgLength forHTTPHeaderField: @"Content-Length"];
    [req setHTTPMethod:@"POST"];
    [req setHTTPBody:[soapMsg dataUsingEncoding:NSUTF8StringEncoding]];
    
    NSURLResponse * response = nil;
    NSError * error = nil;
    NSData * responseData = [NSURLConnection sendSynchronousRequest:req
                                                  returningResponse:&response
                                                              error:&error];
    
    TFHpple *authenParser = [TFHpple hppleWithHTMLData:responseData];
    NSString *xpathQuerySessionID = @"//return";
    NSArray *returnNodes = [authenParser searchWithXPathQuery:xpathQuerySessionID];
    NSMutableString *sessionID = [NSMutableString new];
    
    for (TFHppleElement *element in returnNodes) {
        [sessionID setString:[[element firstChild] content]];
    }
    
    return sessionID;
    //print out session ID
    //NSString *authenResponse = [[NSString alloc]initWithData:(responseData) encoding:NSUTF8StringEncoding];
    //NSLog(@"the content is: %@", sessionID);
}

- (void) closeSession:(NSString*)sessionID{
    NSString *serviceEndPoint = @"http://search.webofknowledge.com/esti/wokmws/ws/WOKMWSAuthenticate/wdsl";
    NSString *actionClose = @"";
    NSMutableString *cookieValue = [NSMutableString stringWithString:[@"SID=" stringByAppendingString:sessionID]];
    NSString *soapMsg = [NSString stringWithFormat:@"<?xml version=\"1.0\" encoding=\"utf-8\"?>\n"
                         "<soapenv:Envelope xmlns:soapenv=\"http://schemas.xmlsoap.org/soap/envelope/\">\n"
                         "<soapenv:Body>\n"
                         "<WOKMWSAuthentcate:closeSession xmlns:WOKMWSAuthentcate=\"http://auth.cxf.wokmws.thomsonreuters.com\"/>"
                         "</soapenv:Body>\n"
                         "</soapenv:Envelope>\n"];
    
    NSURL *soapUrl = [NSURL URLWithString: serviceEndPoint];
    NSMutableURLRequest *req = [NSMutableURLRequest requestWithURL:soapUrl];
    NSString *msgLength = [NSString stringWithFormat: @"%d", (int)[soapMsg length]];
    [req addValue:cookieValue forHTTPHeaderField:@"Cookie"];
    [req addValue: @"text/xml; charset=utf-8" forHTTPHeaderField: @"Content-Type"];
    [req addValue: actionClose forHTTPHeaderField: @"SOAPAction"];
    [req addValue: msgLength forHTTPHeaderField: @"Content-Length"];
    [req setHTTPMethod:@"POST"];
    [req setHTTPBody:[soapMsg dataUsingEncoding:NSUTF8StringEncoding]];
    
    NSURLResponse * response = nil;
    NSError * error = nil;
    NSData * responseData = [NSURLConnection sendSynchronousRequest:req
                                                  returningResponse:&response
                                                              error:&error];
    
    TFHpple *authenParser = [TFHpple hppleWithHTMLData:responseData];
    NSString *xpathQuerySessionID = @"//faultstring";
    NSArray *returnNodes = [authenParser searchWithXPathQuery:xpathQuerySessionID];
    NSMutableString *closeResult = [NSMutableString new];
    
    for (TFHppleElement *element in returnNodes) {
        [closeResult setString:[[element firstChild] content]];
    }
    
    if (![closeResult isEqualToString:@""]) NSLog(@"close fasiled: %@", closeResult);
    else NSLog(@"close successed!");
    //NSString *closeResult2 = [[NSString alloc]initWithData:(responseData) encoding:NSUTF8StringEncoding];
    //NSLog(@"The close session result is: %@", closeResult2);
    
    //return closeResult;
    
}
- (void)analyseURL_WoSAt:(NSInteger)findNo withSession:(NSString*)sessionID andSetResults:(NSMutableArray*)results{
    //Send requests
    NSString *serviceEndPoint = @"http://search.webofknowledge.com/esti/wokmws/ws/WokSearchLite?wsdl";
    NSMutableString *cookieValue = [NSMutableString stringWithString:[@"SID=" stringByAppendingString:sessionID]];
    NSMutableString *suspectTitle = [[NSMutableString alloc] initWithString:self.paperTitle];
    NSInteger loopNO = findNo;
    NSString *actionSearch = @"";
    NSString *soapMsg = [NSString stringWithFormat:@"<?xml version=\"1.0\" encoding=\"utf-8\"?>\n"
                         "<soapenv:Envelope xmlns:soapenv=\"http://schemas.xmlsoap.org/soap/envelope/\" xmlns:woksearchlite=\"http://woksearchlite.v3.wokmws.thomsonreuters.com\">\n"
                         "<soapenv:Header/>\n"
                         "<soapenv:Body>\n"
                         "<woksearchlite:search>\n"
                         "<queryParameters>\n"
                         "<databaseId>WOS</databaseId>\n"
                         "<userQuery>TS=(\"%@\")</userQuery>\n"
                         "<editions>\n"
                         "<collection>WOS</collection>\n"
                         "<edition>SCI</edition>\n"
                         "</editions>\n"
                         "<timeSpan>\n"
                         "<begin>1990-01-01</begin>\n"
                         "<end>2013-12-31</end>\n"
                         "</timeSpan>\n"
                         "<queryLanguage>en</queryLanguage>\n"
                         "</queryParameters>\n"
                         "<retrieveParameters>\n"
                         "<firstRecord>1</firstRecord>\n"
                         "<count>5</count>\n"
                         "</retrieveParameters>\n"
                         "</woksearchlite:search>\n"
                         "</soapenv:Body>\n"
                         "</soapenv:Envelope>\n"
                         , suspectTitle];
    
    NSURL *soapUrl = [NSURL URLWithString: serviceEndPoint];
    NSMutableURLRequest *req = [NSMutableURLRequest requestWithURL:soapUrl];
    NSString *msgLength = [NSString stringWithFormat: @"%d", (int)[soapMsg length]];
    [req addValue:cookieValue forHTTPHeaderField:@"Cookie"];
    [req addValue: @"text/xml; charset=utf-8" forHTTPHeaderField: @"Content-Type"];
    [req addValue: actionSearch forHTTPHeaderField: @"SOAPAction"];
    [req addValue: msgLength forHTTPHeaderField: @"Content-Length"];
    [req setHTTPMethod:@"POST"];
    [req setHTTPBody:[soapMsg dataUsingEncoding:NSUTF8StringEncoding]];
    
    NSURLResponse * response = nil;
    NSError * error = nil;
    sleep(1);
    NSData * responseData = [NSURLConnection sendSynchronousRequest:req
                                                  returningResponse:&response
                                                              error:&error];
    
    /*if ([self.paperTitle isEqualToString:@"Collaborative scientific data visualization"]) {
        NSString *searchResults = [[NSString alloc]initWithData:(responseData) encoding:NSUTF8StringEncoding];
        NSLog(@"The search result is: %@", searchResults);
    }*/
    
    TFHpple *resultParser = [TFHpple hppleWithXMLData:responseData];
    NSString *xpathQueryTitle = @"//title/value";
    NSString *xpathQueryFound = @"//recordsFound";
    NSString *xpathQueryYear = @"//source/value";
    NSArray *returnNodes = [resultParser searchWithXPathQuery:xpathQueryTitle];
    NSArray *foundNodes = nil;
    foundNodes = [resultParser searchWithXPathQuery:xpathQueryFound];
    NSArray *yearNodes = [resultParser searchWithXPathQuery:xpathQueryYear];
    NSMutableString *foundReuslts = [NSMutableString new];
    comparator *wosComp = [comparator new];
    
    for (TFHppleElement *element in foundNodes) {
        [foundReuslts setString:[[element firstChild] content]];
    }
    
    if ([foundReuslts isEqualToString:@"0"]) {
        NSLog(@"Didn't find!");
        return;
    }else if ([foundNodes count] == 0){
        NSLog(@"Request Error in WoS line 187");
        return;
    }
    
    for (TFHppleElement *element in returnNodes) {
        NSString *vTitle = [[NSString alloc] initWithString:[[element firstChild] content]];
        if([wosComp compareTitleFromGS:suspectTitle with:vTitle]){
            NSLog(@"Found %@ in Web of sicence",vTitle);
            [[results objectAtIndex:loopNO] replaceObjectAtIndex:1 withObject:@"YES"];
            for (TFHppleElement *element in yearNodes) {
                NSMutableString *unkownValue = [[NSMutableString alloc] initWithString:[[element firstChild] content]];
                if ([wosComp comparePYFromGS:self.gsYear with:unkownValue]) {
                    [[results objectAtIndex:loopNO] replaceObjectAtIndex:2 withObject:@"YES"];
                    NSLog(@"The Published Year is Matched!!");
                }
                //else NSLog(@"The Published Year isn't mached!");
            }
        }
        //NSLog(@"The search result is: %@", wosResults);
    }
}

- (void) verifiedByWoS:(NSMutableArray*)results{
    int findNo = 0;
    
    //get session ID
    NSString *sessionID = [[NSString alloc] initWithString:[self openSession]];
    
    for (NSMutableArray *paperArray in results) {
        if ([[paperArray objectAtIndex:1] isEqualToString:@"YES"]) {
            findNo++;
            continue;
        }
        self.paperTitle = [NSMutableString stringWithString:[paperArray objectAtIndex:0]];
        NSLog(@"Begin to find %d:%@",findNo+1,self.paperTitle);
        [self analyseURL_WoSAt:findNo withSession:sessionID andSetResults:results];
        findNo++;
    }
    
    //close session ID
   [self closeSession:sessionID];
    
}

/*
 - (void)connection:(NSURLConnection *)connection didReceiveResponse:(NSURLResponse *)response
 {
 self.responseData = [[NSMutableData alloc] init];
 NSLog(@"Got Response!");
 }
 
 - (void)connection:(NSURLConnection *)connection didReceiveData:(NSData *)data {
 // Append the new data to the instance variable you declared
 [self.responseData appendData:data];
 NSLog(@"Getting Data...");
 }
 
 - (NSCachedURLResponse *)connection:(NSURLConnection *)connection
 willCacheResponse:(NSCachedURLResponse*)cachedResponse {
 // Return nil to indicate not necessary to store a cached response for this connection
 return nil;
 }
 
 - (void)connectionDidFinishLoading:(NSURLConnection *)connection {
 // The request is complete and data has been received
 // You can parse the stuff in your instance variable now
 NSLog(@"Finished Loading!");
 NSString *response = [[NSString alloc]initWithData:(self.responseData) encoding:NSUTF8StringEncoding];
 NSLog(@"the content is: %@", response);
 }
 
 - (void)connection:(NSURLConnection *)connection didFailWithError:(NSError *)error {
 // The request has failed for some reason!
 // Check the error var
     NSLog(@"The request has been failed.");
 }
*/

@end
