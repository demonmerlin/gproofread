//
//  Scopus.m
//  GProofread
//
//  Created by Johnny on 27/03/2014.
//  Copyright (c) 2014 Johnny. All rights reserved.
//

#import "Scopus.h"
#import "Track.h"
#import "TFHpple.h"
#import "comparator.h"

@implementation Scopus

- (id)initWithTrack:(Track*)trackFromDelegate
{
    self = [super init];
    if (self)
    {
        self.gsWord = [[NSString alloc] initWithString:trackFromDelegate.wKey];
        self.gsYear = [[NSString alloc] initWithString:trackFromDelegate.year];
    }
    return self;
}

- (void)analyseURL_ScopAt:(NSInteger)findNo andSetResults:(NSMutableArray*)results{
    //api.elsevier.com/content/search/scopus
    NSMutableString *suspectTitle = [[NSMutableString alloc] initWithString:self.paperTitle];
    NSInteger loopNO = findNo;
    comparator *scopComp = [comparator new];
    NSString *url_prefix = @"http://api.elsevier.com/content/search/scidir?query='";
    NSMutableString *url_address = [[NSMutableString alloc] initWithString:[url_prefix stringByAppendingString:suspectTitle]];
    [url_address appendString:@"'"];
    NSString *properlyEscapedURL = [url_address stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
    
    NSURL *url = [NSURL URLWithString:properlyEscapedURL];
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL: url];
    [request setValue:@"application/xml" forHTTPHeaderField:@"Accept"];
    [request setValue:@"0ac58006f10a290ad307c358c03eb968" forHTTPHeaderField:@"X-ELS-APIKey"];
    
    //Sync
    NSURLResponse * response = nil;
    NSError * error = nil;
    sleep(1);
    NSData * responseData = [NSURLConnection sendSynchronousRequest:request
                                                  returningResponse:&response
                                                              error:&error];
    if (error != nil) {
        NSLog(@"error %@",error);
    }
    
    //opensearch:totalResults
    TFHpple *resultParser = [TFHpple hppleWithXMLData:responseData];
    NSString *xpathQueryFound = @"//*[name()='opensearch:totalResults']";
    NSString *xpathQueryTitle = @"//*[name()='dc:title']";
    NSString *xpathQueryYear = @"//*[name()='prism:coverDate']";
    
    NSArray *foundNodes = nil;
    foundNodes = [resultParser searchWithXPathQuery:xpathQueryFound];
    NSArray *returnNodes = [resultParser searchWithXPathQuery:xpathQueryTitle];
    NSArray *yearNodes = [resultParser searchWithXPathQuery:xpathQueryYear];
    
    NSMutableString *foundReuslts = [NSMutableString new];
    for (TFHppleElement *element in foundNodes) {
        [foundReuslts setString:[[element firstChild] content]];
    }
    
    if ([foundReuslts isEqualToString:@"0"]) {
        NSLog(@"Didn't find!");
        return;
    }else if ([foundNodes count] == 0){
        NSLog(@"Request Error in Scopus line 73");
        return;
    }
    int yearCount = 0;
    for (TFHppleElement *element in returnNodes) {
        NSInteger oddOReven = 0;
        NSString *vTitle = [[NSString alloc] initWithString:[[element firstChild] content]];
        if([scopComp compareTitleFromGS:suspectTitle with:vTitle]){
            NSLog(@"Found %@ in Scopus",vTitle);
            [[results objectAtIndex:loopNO] replaceObjectAtIndex:1 withObject:@"YES"];
            int forCount = 0;
            for (TFHppleElement *yearElement in yearNodes) {
                if (forCount==yearCount) {
                    if (oddOReven%2==0) {//Because there are two coverDate in Scopus' results
                        NSMutableString *unkownValue = [[NSMutableString alloc] initWithString:[[yearElement firstChild] content]];
                        if ([scopComp comparePYFromGS:self.gsYear with:unkownValue]) {
                            [[results objectAtIndex:loopNO] replaceObjectAtIndex:2 withObject:@"YES"];
                            NSLog(@"The Published Year is Matched!!");
                        }
                    }
                }
                //else NSLog(@"The Published Year isn't mached!");
                forCount++;
            }
        }
        //NSLog(@"The search result is: %@", wosResults);
        yearCount++;
    }

    //NSString *searchResult = [[NSString alloc]initWithData:(responseData) encoding:NSUTF8StringEncoding];
    //NSLog(@"The Scopus result is: %@", searchResult);
}

- (void) verifiedByScop:(NSMutableArray*)results{
    int findNo = 0;
    for (NSMutableArray *paperArray in results) {
        if ([[paperArray objectAtIndex:1] isEqualToString:@"YES"]) {
            findNo++;
            continue;
        }
        self.paperTitle = [NSMutableString stringWithString:[paperArray objectAtIndex:0]];
        NSLog(@"Begin to find %d:%@",findNo+1,self.paperTitle);
        [self analyseURL_ScopAt:findNo andSetResults:results];
        findNo++;
    }
    
    NSLog(@"Finishing Comparing");
}

@end
