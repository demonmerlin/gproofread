//
//  comparator.h
//  GProofread
//
//  Created by Johnny on 24/03/2014.
//  Copyright (c) 2014 Johnny. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface comparator : NSObject

- (BOOL) compareTitleFromGS:(NSMutableString*)GStitle with:(NSString*)vTitle;
- (BOOL) comparePYFromGS:(NSString*)GSPY with:(NSMutableString*)vPY;

@end
