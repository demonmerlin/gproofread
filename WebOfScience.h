//
//  WebOfScience.h
//  GProofread
//
//  Created by Johnny on 25/03/2014.
//  Copyright (c) 2014 Johnny. All rights reserved.
//


@class Track;
@class TFHpple;

#import <Foundation/Foundation.h>

@interface WebOfScience : NSObject <NSURLConnectionDelegate>

@property NSString *gsWord;
@property NSString *gsYear;
@property NSMutableString *paperTitle;
//@property BOOL titleFlag;
//@property BOOL yearFlag;
//@property NSString *wosTitle;
//@property NSString *wosYear;
//@property NSMutableArray *wosPaperArray;
//@property NSMutableArray *wosYearArray;
//@property NSInteger countEntrance;

- (id)initWithTrack:(Track*)trackFromDelegate;
- (void) verifiedByWoS:(NSMutableArray*)results;
- (void)analyseURL_WoSAt:(NSInteger)findNo withSession:(NSString*)sessionID andSetResults:(NSMutableArray*)results;
- (NSMutableString*) openSession;
- (void) closeSession: (NSString*)sessionID;


/*
- (void)connection:(NSURLConnection *)connection didReceiveResponse:(NSURLResponse *)response;
- (void)connection:(NSURLConnection *)connection didReceiveData:(NSData *)data;
- (NSCachedURLResponse *)connection:(NSURLConnection *)connection;
- (void)connectionDidFinishLoading:(NSURLConnection *)connection;
- (void)connection:(NSURLConnection *)connection didFailWithError:(NSError *)error;
 */
@end
